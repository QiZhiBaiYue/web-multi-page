const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const {entryBuild, htmlWebpackPluginBuild} = require('./webpack.util');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const glob = require('glob')
const CONFIG = require('../../global.config.json');
const {resolve} = path;


module.exports = {
    entry: entryBuild(glob.sync(resolve(__dirname, '../src/js/view/*.js'))),
    output: {
        path: resolve(__dirname, `../${CONFIG.DIR.DIST}`),
        publicPath: CONFIG.PATH.PUBLIC_PATH,
        filename: `${CONFIG.DIR.SCRIPT}/[name].bundle.js`,
        chunkFilename: `${CONFIG.DIR.SCRIPT}/[name].[chunkhash].js`
    },
    //将错误信息准确的定位
    devtool: 'inline-source-map',
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: `${CONFIG.DIR.STYLE}/[name].css`
        }),
        ...glob.sync(resolve(__dirname, '../src/views/*.ejs')).map(htmlWebpackPluginBuild)
    ],
    resolve: {
        alias: {
            '@': resolve(__dirname, '../src'),
        }
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    minChunks: 1,
                    chunks: 'all',
                    priority: 100
                }
            }
        },
        runtimeChunk: {
            name: 'manifest'
        }
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "less-loader"
                    },
                    {
                        loader: "postcss-loader"
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "sass-loader"
                    },
                    {
                        loader: "postcss-loader"
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: "css-loader"
                    }, {
                        loader: "postcss-loader"
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        // loader: 'url-loader',
                        // options: {
                        //     name: '[name].[hash:5].[ext]',
                        //     limit: 2048,
                        //     outputPath: CONFIG.DIR.IMAGE
                        // }
                        loader: 'file-loader',
                        options: {
                            name: '[name].[hash:5].[ext]',
                            outputPath: CONFIG.DIR.IMAGE,
                        }
                    },
                ]
            },
            {
                test: /\.(eot|woff2|woff|ttf|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: CONFIG.DIR.FONT
                        }
                    },
                ]
            },
            {
                test: /\.(csv|tsv)$/,
                use: [
                    'csv-loader'
                ]
            },
            {
                test: /\.xml$/,
                use: [
                    'xml-loader'
                ]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            attributes: {
                                list: [
                                    {
                                        tag: 'img',
                                        attribute: 'src',
                                        type: 'src',
                                    },
                                    {
                                        tag: 'img',
                                        attribute: 'srcset',
                                        type: 'srcset',
                                    },
                                    {
                                        tag: 'img',
                                        attribute: 'data-src',
                                        type: 'src',
                                    },
                                    {
                                        tag: 'img',
                                        attribute: 'data-srcset',
                                        type: 'srcset',
                                    },
                                ],
                            }
                        }
                    },
                ]
            },
            {
                test: /\.ejs$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            attributes: {
                                list: [
                                    {
                                        tag: 'img',
                                        attribute: 'src',
                                        type: 'src',
                                    },
                                    {
                                        tag: 'img',
                                        attribute: 'srcset',
                                        type: 'srcset',
                                    },
                                    {
                                        tag: 'img',
                                        attribute: 'data-src',
                                        type: 'src',
                                    },
                                    {
                                        tag: 'img',
                                        attribute: 'data-srcset',
                                        type: 'srcset',
                                    },
                                ],
                            }
                        }
                    },
                    {
                        loader: 'ejs-html-loader',
                        options: {
                            production: process.env.ENV === 'production'
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader'
            }
        ]
    },
};
