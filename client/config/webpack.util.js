const HtmlWebpackPlugin = require('html-webpack-plugin');
const CONFIG = require('../../global.config.json');
const isDev = process.env.NODE_ENV === 'development'

/**
 * 入口文件统一编译入口
 * @param filepathList
 */
exports.entryBuild = (filepathList) => {
    let entry = {}
    filepathList.forEach(filepath => {
        const list = filepath.split(/[\/|\/\/|\\|\\\\]/g)
        const key = list[list.length - 1].replace(/\.js/g, '')
        entry[key] = filepath;
    })
    return entry
}

/**
 * 出口文件统一编译出口
 * @param filepath
 * @returns {HtmlWebpackPlugin}
 */
exports.htmlWebpackPluginBuild = (filepath) => {
    const tempList = filepath.split(/[\/|\/\/|\\|\\\\]/g)
    // 读取 CONFIG.EXT 文件自定义的文件后缀名，默认生成 ejs 文件，可以定义生成 html 文件
    const filename = (name => `${name.split('.')[0]}.${CONFIG.EXT}`)(`${CONFIG.DIR.VIEW}/${tempList[tempList.length - 1]}`)
    const template = filepath
    const fileChunk = filename.split('.')[0].split(/[\/|\/\/|\\|\\\\]/g).pop()
    const chunks = isDev ? [fileChunk] : ['manifest', 'vendors', fileChunk]
    return new HtmlWebpackPlugin({filename, template, chunks})
}
