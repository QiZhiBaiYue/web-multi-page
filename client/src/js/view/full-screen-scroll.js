import '../../assets/styles/full-screen-scroll.less'
import {ScrollCapture} from "../utils";
import {gsap} from 'gsap'

const sections = gsap.utils.toArray('section');
let sectionIndex = 0;

function setSection(newIndex = 0) {
    if (newIndex !== sectionIndex && newIndex < sections.length && newIndex >= 0) {
        const currentSection = sections[sectionIndex];
        const newSection = sections[newIndex];
        gsap.to(currentSection, {scale: 0.8, autoAlpha: 0})
        gsap.to(newSection, {scale: 1, autoAlpha: 1});
        sectionIndex = newIndex;
    }
}

const scrollCapture = new ScrollCapture();
scrollCapture.handleScrollUp = () => {
    setSection(sectionIndex - 1)
}
scrollCapture.handleScrollDown = () => {
    setSection(sectionIndex + 1)
}
