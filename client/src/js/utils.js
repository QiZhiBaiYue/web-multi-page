/**
 * 鼠标移动捕捉器
 * @constructor
 */
export function ScrollCapture() {
    /**
     * 鼠标滑动事件防抖动
     * @param callback
     * @param wait
     */
    this.debounce = (callback, wait = 3000) => {
        let timer;
        return function () {
            let ctx = this;
            let args = arguments;

            if (timer) {
                clearTimeout(timer)
            }

            timer = setTimeout(() => {
                callback.apply(ctx, args)
            }, wait);
        }
    }

    /**
     * 鼠标滑动事件处理
     * @param ev
     */
    this.handleScrollEnd = (ev) => {
        if (ev.deltaY > 0 && this.handleScrollDown) {
            this.handleScrollDown();
        } else if (ev.deltaY < 0 && this.handleScrollUp) {
            this.handleScrollUp();
        }
    }

    // 鼠标上滑触发事件
    this.handleScrollUp = null;
    // 鼠标下滑触发事件
    this.handleScrollDown = null;

    window.addEventListener('mousewheel', this.debounce(this.handleScrollEnd, 200));
}
