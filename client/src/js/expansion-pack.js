/**
 * 创建人: Harry Wen
 * 创建时间: 2020/8/17 11:33
 * 描述：该文件内只包含对于原生的一些对象扩展，请不要改写原生对象的方法
 **/

/**
 * @author Harry Wen
 * @param val
 * @returns {boolean}
 * @description 比较两个浮点数是否相等
 * @throws Error
 */
Number.prototype.equalsFloat = function (val) {
    if (typeof  val === "number") {
        return Math.abs(this - val) <= Number.EPSILON;
    }
    throw new Error('请确认参与对比的浮点数类型是否为数字');
}
