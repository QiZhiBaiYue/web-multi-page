const RenderContext = require('../utils/render');

module.exports = () => {
    return async function notFoundHandler(ctx, next) {
        await next();
        if (ctx.status === 404 && !ctx.body) {
            if (ctx.acceptJSON) {
                ctx.body = { error: 'Not Found' };
            } else {
                const renderContext = new RenderContext(ctx);
                ctx.body = await renderContext.renderEjs('404.ejs');
            }
        }
    };
};
