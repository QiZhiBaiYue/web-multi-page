const {RequestError} = require('../models/error')

// 状态检查
function checkStatus(httpResponse) {
    if (!httpResponse) {
        throw new RequestError('HttpResponse异常', httpResponse);
    }
    if (!Number.isInteger(httpResponse.status)) {
        throw new RequestError('HttpResponse状态异常', httpResponse);
    }
    const {status} = httpResponse;
    if (!(status >= 200 && status <= 300)) {
        throw  new RequestError('HttResponse状态异常', httpResponse);
    }
}

/**
 * 检查responseBody
 * @param body
 */
function checkResponseBody(body) {
    if (typeof body === "object") {
        if (body.code) {
            return body.data;
        } else {
            throw new Error(body.message || '数据获取失败');
        }
    }
    throw new Error(body.message || '接口服务器响应数据无效');
}

function RequestFactory(ctx) {
    this.ctx = ctx;
}

/**
 * 参数检查
 * @param url
 * @param option
 */
RequestFactory.prototype.checkParams = function ({url, option = {}}) {
    if (!this.ctx) throw new Error('http操作接口不存在');
    if (!(url && typeof url === "string")) throw new Error('资源地址不存在');
    if (!(option && typeof option === 'object')) throw new Error('请求option设置类型错误');
}

RequestFactory.prototype.getText = async function ({url, option = {}, params}) {
    this.checkParams({url, option});
    let paramsStr = '';
    if (params && typeof params === "object") {
        paramsStr = new URLSearchParams(params).toString();
    }
    const result = await this.ctx.curl(url + paramsStr, {
        dataType: 'text',
        ...option
    });
    checkStatus(result);
    return result;
}

RequestFactory.prototype.get = async function ({url, option = {}, params}) {
    this.checkParams({url, option});
    let paramsStr = '';
    if (params && typeof params === "object") {
        paramsStr = new URLSearchParams(params).toString();
    }
    const realUrl = `${url}${paramsStr ? '?' + paramsStr : ''}`;
    const result = await this.ctx.curl(realUrl, {
        dataType: 'json',
        ...option
    });
    checkStatus(result);
    return checkResponseBody(result.data);
}

RequestFactory.prototype.post_json = async function ({url, option = {}, params}) {
    this.checkParams({url, option});
    const result = await this.ctx.curl(url, {
        dataType: 'json',
        method: 'POST',
        contentType: 'json',
        data: typeof params == "object" ? params : {},
        ...option
    });
    checkStatus(result);
    return checkResponseBody(result.data);
}

module.exports = {
    RequestFactory
}
