const {RequestFactory} = require('./request')
const CONFIG = require('../../global.config.json')
const has = require('lodash').has;

/**
 * 用于在渲染引擎中使用的工具函数
 * @constructor
 */
function RenderUtils() {
    // 根据对象路径查找该值是否存在
    this.hasInPath = function (key) {
        if (typeof key !== "string") {
            return false;
        }
        return has(this, key);
    }
    // 目标的url是否与当前的url相同
    this.isCurrentUrl = function (targetUrl) {
        if (typeof targetUrl === "string") {
            const realUrl = this.ctx.originalUrl.split('?')[0];
            return realUrl.indexOf(targetUrl) !== -1;
        }
        return false;
    }
    // 图片路径拼接
    this.getImageAbsolutePath = function (imgRelativePath) {
        return this.ctx.app.config.serviceUri + imgRelativePath;
    }
}

/**
 * 渲染工具类
 * @param ctx
 * @constructor
 */
function RenderContext(ctx) {
    this.ctx = ctx;
}

/**
 * 渲染 ejs模板
 * @param res ejs文件名
 * @param data 要渲染的数据
 * @returns {Promise<string|any|undefined>}
 */
RenderContext.prototype.renderEjs = async function renderEjs(res, data) {
    if (!this.ctx) throw new Error('RenderContext缺少ctx上下文对象');
    if (!res) throw new Error('未指定要渲染的res对象');
    if (this.ctx.app.env === 'prod') {
        return this.ctx.renderView(res, {...data, ...new RenderUtils()});
    } else {
        const result = await new RequestFactory(this.ctx).getText({url: `http://127.0.0.1:${CONFIG.PORT}${CONFIG.PATH.PUBLIC_PATH}${CONFIG.DIR.VIEW}/${res}`});
        return this.ctx.renderString(result.data, {...data, ...new RenderUtils()});
    }
}


module.exports = RenderContext
