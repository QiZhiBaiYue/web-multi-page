module.exports = app => {
    const {router, controller} = app;
    router.get('/', controller.home.index);
    router.get('/full-screen-scroll',controller.home.fullScreenScroll)
}
