function RequestError(message,target){
    this.message = message;
    this.target = target;
}

RequestError.prototype = new Error('');

module.exports = {
    RequestError
}
