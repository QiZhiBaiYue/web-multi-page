const Controller = require('egg').Controller;
const RenderContext = require('../utils/render');

class HomeController extends Controller {
    async index() {
        const renderContext = new RenderContext(this.ctx);
        this.ctx.body = await renderContext.renderEjs('index.ejs', {
            title: 'demo',
        })
    }

    async fullScreenScroll() {
        const renderContext = new RenderContext(this.ctx);
        this.ctx.body = await renderContext.renderEjs('full-screen-scroll.ejs')
    }
}

module.exports = HomeController;
