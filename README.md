# web-multi-page

#### 介绍
该项目是一个多页面解决方案，主要使用了nodejs作为服务器

#### 软件架构
* [webpack](https://webpack.js.org)
    > 主要用于打包客户端代码
* [nodejs](https://nodejs.org/)
    > 作为服务器主要环境
* [egg.js](https://eggjs.org/)
    >一个nodejs平台的高性能web框架
* [ejs](https://ejs.bootcss.com/)
    >一个模板引擎


#### 安装教程

1. yarn install
2. yarn run dev

#### 打包发布

1. yarn run build
2. yarn run build-server

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
