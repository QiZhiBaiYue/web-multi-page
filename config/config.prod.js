const path = require('path');
const CONFIG = require('../global.config.json')

module.exports = appInfo => {
    return {
        view: {
            root: path.join(appInfo.baseDir, `client/${CONFIG.DIR.DIST}/${CONFIG.DIR.VIEW}`)
        },
        static: {
            prefix: '/public/',
            dir: path.resolve(appInfo.baseDir, `client/${CONFIG.DIR.DIST}`)
        },
    }
}
