module.exports = {
    keys: '__web_multi_page__',
    view: {
        mapping: {
            '.ejs': 'ejs',
        },
        defaultViewEngine: 'ejs',
    },
    httpclient: {
        request: {
            // 默认 request 超时时间
            timeout: 30000,
        },
    },
    middleware: [ 'notfoundHandler' ],
};
