const path = require('path');
const CONFIG = require('../global.config.json');

module.exports = appInfo => {
    return {
        webpack : {
            port: CONFIG.PORT,
            browser: true,
            webpackConfigList: [require(path.resolve(__dirname, '../client/config/webpack.config.js'))],
            // proxy: {
            //     host: 'http://127.0.0.1:9000', // target host that matched path will be proxy to
            //     match: /^\/public\// // proxy url path pattern.
            // },
        },
    }
}
