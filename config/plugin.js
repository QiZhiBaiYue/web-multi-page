exports.webpack = {
    enable: true,
    package: 'egg-webpack'
}
exports.ejs = {
    enable: true,
    package: 'egg-view-ejs',
};

exports.static = {
    enable: true,
    package: 'egg-static'
}
